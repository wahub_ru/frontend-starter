"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp"
import gulpif from "gulp-if"
import debug from "gulp-debug"
import browsersync from "browser-sync"
import yargs from "yargs"
import sourcemaps from "gulp-sourcemaps";
import plumber from "gulp-plumber";
import rename from "gulp-rename";
import rigger from "gulp-rigger";
import uglify from "gulp-uglify";

const argv = yargs.argv,
    production = !!argv.production;

gulp.task("scripts", () => {
    return gulp.src(paths.scripts.src)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulpif(!production, sourcemaps.init()))
        .pipe(gulpif(production, uglify()))
        .pipe(gulpif(production, rename({
            suffix: ".min"
        })))
        .pipe(plumber.stop())
        .pipe(gulpif(!production, sourcemaps.write()))
        .pipe(gulp.dest(paths.scripts.dist))
        .pipe(gulpif(production, debug({
            "title": `JS Production =>`
        })))
        .pipe(gulpif(!production, debug({
            "title": `Работа с JS  =>`
        })))
        .on("end", browsersync.reload);
});
