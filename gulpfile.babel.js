"use strict";

import gulp from "gulp";

const requireDir = require("require-dir"),
    paths = {
        views: {
            src: "./src/views/*.html",
            dist: "./dist/",
            watch: "./src/views/**/*.html"
        },
        styles: {
            src: ["./src/styles/*.{scss,sass}", "./src/styles/vendor/**/*.{scss,sass}"],
            dist: "./dist/css/",
            watch: "./src/styles/**/*.{scss,sass}"
        },
        scripts: {
            src: "./src/js/**/*.js",
            dist: "./dist/js/",
            watch: "./src/js/**/*.js"
        },
        images: {
            src: [
                "./src/img/**/*.{jpg,jpeg,png,gif,tiff,svg}",
                "!./src/img/favicon/*.{jpg,jpeg,png,gif,tiff}"
            ],
            dist: "./dist/img/",
            watch: "./src/img/**/*.{jpg,jpeg,png,gif,svg,tiff}"
        },
        sprites: {
            src: "./src/img/svg/*.svg",
            dist: "./dist/img/sprites/",
            watch: "./src/img/svg/*.svg"
        },
        fonts: {
            src: "./src/fonts/**/*.{woff,woff2}",
            dist: "./dist/fonts/",
            watch: "./src/fonts/**/*.{woff,woff2}"
        }
    };

requireDir("./gulp-tasks/");

export { paths };

export const development = gulp.series("clean",
    gulp.parallel(["views", "styles", "scripts", "images", "sprites", "fonts"]),
    gulp.parallel("serve"));

export const prod = gulp.series("clean",
    gulp.series(["views", "styles", "scripts", "images", "sprites", "fonts"]));

export default development;